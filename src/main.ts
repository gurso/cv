import "./style.css"

const age = new Date().getFullYear() - new Date(1987, 0, 23, 5, 37).getFullYear()
document.getElementById("age")!.textContent = age.toString()
document.getElementById("exp")!.textContent = (age - 28).toString()

function getOffsetParent(el: HTMLElement) {
	if (el.offsetParent) return el.offsetParent
	do {
		el = el.parentElement!
	} while (el !== document.documentElement && getComputedStyle(el).position === "static")
	return el
}

function grab(el: HTMLElement) {
	const parent = getOffsetParent(el)
	let grabbing = false,
		offsetX = 0,
		offsetY = 0
	let hooks = Array.from(el.getElementsByClassName("grab")) as HTMLElement[]
	if (hooks.length === 0) hooks = [el]
	for (const hook of hooks) {
		hook.addEventListener("mousedown", (e: MouseEvent) => {
			const bcr = el.getBoundingClientRect()
			grabbing = true
			offsetX = e.clientX - bcr.x
			offsetY = e.clientY - bcr.y
		})
	}
	document.body.addEventListener("mousemove", e => {
		if (grabbing) {
			const bcrParent = parent.getBoundingClientRect()
			el.style.left = e.clientX - bcrParent.x - offsetX + "px"
			el.style.top = e.clientY - bcrParent.y - offsetY + "px"
		}
	})
	document.body.addEventListener("mouseup", () => (grabbing = false))
}
for (const li of document.getElementById("skills")!.children) {
	grab(li as HTMLElement)
}
